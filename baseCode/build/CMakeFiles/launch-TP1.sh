#!/bin/sh
bindir=$(pwd)
cd /home/e20190008448/Bureau/MoteurDeJeu/moteur-de-jeu/baseCode/TP1/
export 

if test "x$1" = "x--debugger"; then
	shift
	if test "xYES" = "xYES"; then
		echo "r  " > $bindir/gdbscript
		echo "bt" >> $bindir/gdbscript
		/usr/bin/gdb -batch -command=$bindir/gdbscript --return-child-result /home/e20190008448/Bureau/MoteurDeJeu/moteur-de-jeu/baseCode/build/TP1 
	else
		"/home/e20190008448/Bureau/MoteurDeJeu/moteur-de-jeu/baseCode/build/TP1"  
	fi
else
	"/home/e20190008448/Bureau/MoteurDeJeu/moteur-de-jeu/baseCode/build/TP1"  
fi
