#include "scene.hpp"

Scene::Scene(){
}

void Scene::addChild(GameObject* gameO){
    this->objectChilds.push_back(gameO);
}

void Scene::setupScene(){
    for(unsigned long int i = 0; i < objectChilds.size(); i++){
        objectChilds[i]->setup();
    }
}

void Scene::drawScene(GLint programID){
    for(unsigned long int i = 0; i < objectChilds.size(); i++){
        objectChilds[i]->draw(programID);
    }
}