#include "transform.hpp"
// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>
// Include GLM
#include <glm/glm.hpp>

////////////////////////CONSTRUCTORS///////////////////////
Transform::Transform(){
    this->position = glm::vec3(0,0,0);
	this->rotate = glm::vec3(0,0,0);
	this->scale = glm::vec3(1,1,1);

}
Transform::Transform(glm::vec3 pos, glm::vec3 rotate, glm::vec3 scale){
    this->position = pos;
    this->rotate = rotate;
    this->scale = scale;
}

void Transform::setTransform(glm::vec3 pos, glm::vec3 rotate, glm::vec3 scale){
    this->position = pos;
    this->rotate = rotate;
    this->scale = scale;
}

////////////////////GETTERS////////////////////////////
glm::vec3 Transform::getPosition(){
    return this->position;
}
glm::vec3 Transform::getRotate(){
    return this->rotate;
}
glm::vec3 Transform::getScale(){
    return this->scale;
}

//////////////////SETTERS/////////////////////////
void Transform::setPosition(glm::vec3 position){
    this->position = position;
}
void Transform::setRotate(glm::vec3 rotate){
    this->rotate = rotate;
}
void Transform::setScale(glm::vec3 scale){
    this->scale = scale;
}

/////////////////CHANGES/////////////////////////
void Transform::changePosition(glm::vec3 position){
    this->position += position;
}
void Transform::changeRotate(glm::vec3 rotate){
    this->rotate += rotate;
}
void Transform::changeScale(glm::vec3 scale){
    this->scale += scale;
}

///////////////////////////////////////////////
void Transform::updateModelMatrix(Transform* parent){
	
    modelMatrix = glm::translate(glm::mat4(1.0f), getPosition())
                * glm::rotate<float>(glm::mat4(1.0f), getRotate().y, glm::vec3(1,0,0))
                * glm::rotate<float>(glm::mat4(1.0f), getRotate().z, glm::vec3(0,1,0))
                * glm::rotate<float>(glm::mat4(1.0f), getRotate().x, glm::vec3(0,0,1))
                * glm::scale(glm::mat4(1.0f),getScale());

   	modelMatrix = parent->getModelMatrix() * modelMatrix;
}

void Transform::updateModelMatrix(){
	
    modelMatrix = glm::translate(glm::mat4(1.0f), getPosition())
                * glm::rotate<float>(glm::mat4(1.0f), getRotate().y, glm::vec3(1,0,0))
                * glm::rotate<float>(glm::mat4(1.0f), getRotate().z, glm::vec3(0,1,0))
                * glm::rotate<float>(glm::mat4(1.0f), getRotate().x, glm::vec3(0,0,1))
                * glm::scale(glm::mat4(1.0f),getScale());
}

glm::mat4 Transform::getModelMatrix(){
    return this->modelMatrix;
}

