#include "plane.hpp"
#include "utilities.hpp"
#include <string>

float randomFloat()
{
    return (float)(rand()) / (float)(RAND_MAX);
}
 
int randomInt(int a, int b)
{
    if (a > b)  
        return randomInt(b, a);
    if (a == b)
        return a;
    return a + (rand() % (b - a));
}
 
float randomFloat(int a, int b)
{
    if (a > b)
        return randomFloat(b, a);
    if (a == b)
        return a;
 
    return (float)randomInt(a, b) + randomFloat();
}

Plane::Plane(){
}

Plane::Plane(double longueur, double largeur, bool isHighParent){
	this->longueur = longueur;
	this->largeur = largeur;
	this->position = glm::vec3(0,0,0);
	this->nbPointsLargeur = 2;
	this->nbPointsLongueur = 2;
	this->isHighParent = isHighParent;
}

Plane::Plane(double longueur, double largeur, glm::vec3 position, bool isHighParent){
	this->longueur = longueur;
	this->largeur = largeur;
	this->position = position;
	this->nbPointsLargeur = 2;
	this->nbPointsLongueur = 2;
	this->isHighParent = isHighParent;
}

Plane::Plane(double longueur, double largeur, int nbPointsLongueur, int nbPointsLargeur, glm::vec3 position, bool isHighParent){
	this->longueur = longueur;
	this->largeur = largeur;
	this->position = position;
	this->nbPointsLargeur = nbPointsLargeur;
	this->nbPointsLongueur = nbPointsLongueur;
	this->isHighParent = isHighParent;
}

Plane::Plane(double longueur, double largeur, int nbPointsLongueur, int nbPointsLargeur, glm::vec3 position, const char*  HeighMapPath,  bool isHighParent){
	this->longueur = longueur;
	this->largeur = largeur;
	this->position = position;
	this->nbPointsLargeur = nbPointsLargeur;
	this->nbPointsLongueur = nbPointsLongueur;
	this->heighMap = HeighMap(HeighMapPath);
	this->isHighParent = isHighParent;
}

void Plane::loadTextures(std::vector<std::string> colorTextures){
	m_textureHerbe = loadTexture2DFromFilePath(colorTextures[0]);
	m_textureRoche = loadTexture2DFromFilePath(colorTextures[1]);
	m_textureNeige = loadTexture2DFromFilePath(colorTextures[2]);
}

void Plane::bindTextures(){
	glActiveTexture(GL_TEXTURE0 + 0);	
    glBindTexture(GL_TEXTURE_2D, m_textureHerbe);
	glUniform1i(glGetUniformLocation(m_program, "textureHerbe"), 0);
	
	glActiveTexture(GL_TEXTURE0 + 1);
    glBindTexture(GL_TEXTURE_2D, m_textureRoche);
	glUniform1i(glGetUniformLocation(m_program, "textureRoche"), 1);

	glActiveTexture(GL_TEXTURE0 + 2);
    glBindTexture(GL_TEXTURE_2D, m_textureNeige);
	glUniform1i(glGetUniformLocation(m_program, "textureNeige"), 2);
}

void Plane::construct(bool harsardY, bool heighMapActivated){
	indexed_vertices.clear();
	indices.clear();

	double separationLargeur = double(largeur) / nbPointsLargeur;
	double separationLongueur = double(longueur) / nbPointsLongueur;
	int ajout = ((longueur + largeur) / 2)/4;

	for(int i = 0; i < nbPointsLongueur; i++){
		for(int j = 0; j < nbPointsLargeur; j++){

			double posX = (j * separationLargeur - double(largeur)/2) + position.x;
			double posZ = (i * separationLongueur - double(longueur)/2) + position.z;
			double posY = position.y;
			
			if(harsardY){
				float ajoutHasard = randomFloat(posY, ajout);
				posY = position.y + ajoutHasard;
			}

			glm::vec3 nouveauPoint = glm::vec3(posX, posY, posZ);

			indexed_vertices.push_back(nouveauPoint);
		}		
	}

	for(int i = 0; i < nbPointsLongueur - 1; i++){
		for(int j = 0; j < nbPointsLargeur - 1; j++){
			
			int a = i * nbPointsLargeur + j;
			int b = i * nbPointsLargeur + (j + 1);
			int c = (i + 1) * nbPointsLargeur + j;
			int d = (i + 1) * nbPointsLargeur + (j + 1);

			indices.push_back(a);
			indices.push_back(b);
			indices.push_back(d);

			indices.push_back(a);
			indices.push_back(c);
			indices.push_back(d);
		}
	}
	
	this->getUV();

	if(heighMapActivated){
		float hauteurMax = 0;

		for(int i = 0; i < indexed_vertices.size(); i++){
			int Upos = uvs[i][0] * this->heighMap.width;
			int Vpos = uvs[i][1] * this->heighMap.height;

			indexed_vertices[i].y = (this->heighMap.values[Upos + Vpos * this->heighMap.width]/255.0) * 10 + position.y;

			if(hauteurMax < indexed_vertices[i].y){
				hauteurMax = indexed_vertices[i].y;
			}
		}

		std::cout<<hauteurMax<<std::endl;
		glUniform1f(glGetUniformLocation(m_program,"hateurMax"), hauteurMax);
	}

	glGenBuffers(1, &UVbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, UVbuffer);
    glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);
}

void Plane::constructSimple(){
	glm::vec3 a = position + glm::vec3(-largeur/2, +longueur/2, 0); indexed_vertices.push_back(a);
	glm::vec3 b = position + glm::vec3(+largeur/2, +longueur/2, 0);	indexed_vertices.push_back(b);
	glm::vec3 c = position + glm::vec3(-largeur/2, -longueur/2, 0);	indexed_vertices.push_back(c);
	glm::vec3 d = position + glm::vec3(+largeur/2, -longueur/2, 0);	indexed_vertices.push_back(d);

	indices.push_back(0); indices.push_back(2); indices.push_back(3); indices.push_back(0); indices.push_back(1); indices.push_back(3); 
}

void Plane::keys(GLFWwindow *window, bool hasardY, bool heighMapActivated){
	if(glfwGetKey(window, GLFW_KEY_KP_ADD) == GLFW_PRESS){
            this->setNbPointsLargeur(this->getNbPointsLargeur() + 1);
            this->setNbPointsLongeur(this->getNbPointsLongueur() + 1);
            this->construct(hasardY, heighMapActivated);
            this->setup();
        }
    if(glfwGetKey(window, GLFW_KEY_KP_SUBTRACT) == GLFW_PRESS){
            this->setNbPointsLargeur(this->getNbPointsLargeur() - 1);
            this->setNbPointsLongeur(this->getNbPointsLongueur() - 1);
            this->construct(hasardY, heighMapActivated);
            this->setup();
    }
}

void Plane::getUV(){
	for(int i = 0; i < indexed_vertices.size(); i++){
		uvs.push_back(glm::vec2(0,0));
		uvs[i][0] = ((indexed_vertices[i][0]+position[0]) / this->largeur) + 0.5;
		uvs[i][1] = ((indexed_vertices[i][2]+position[2]) / this->longueur) + 0.5;
	}
}

void Plane::loadUVShader(){	
	glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, UVbuffer);
    glVertexAttribPointer(
                1,                  // attribute
                2,                  // size
                GL_FLOAT,           // type
                GL_FALSE,           // normalized?
                0,                  // stride
                (void*)0            // array buffer offset
                );
}

/*--------------GETTERS----------------*/
double Plane::getLongueur(){
	return this->longueur;
}
double Plane::getLargeur(){
	return this->largeur;
}
int Plane::getNbPointsLongueur(){
	return this->nbPointsLongueur;
}
int Plane::getNbPointsLargeur(){
	return this->nbPointsLargeur;
}
glm::vec3 Plane::getPosition(){
	return this->position;
}

/*--------------SETTERS----------------*/
void Plane::setLongueur(double newLongueur){
	this->longueur = newLongueur;
}
void Plane::setLargeur(double newLargeur){
	this->largeur = newLargeur;
}
void Plane::setNbPointsLongeur(int newNbPointsLongeur){
	this->nbPointsLongueur = newNbPointsLongeur;
}
void Plane::setNbPointsLargeur(int newNbPointsLargeur){
	this->nbPointsLargeur = newNbPointsLargeur;
}
void Plane::setPosition(glm::vec3 newPosition){
	this->position = newPosition;
}