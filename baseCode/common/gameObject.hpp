#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>
// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <string>
#include <common/shader.hpp>
#include <common/objloader.hpp>
#include <common/vboindexer.hpp>
#include "transform.hpp"


class GameObject{
	protected:
		std::vector<unsigned short> indices; //Triangles concaténés dans une liste
	    std::vector<std::vector<unsigned short>> triangles;
	    std::vector<glm::vec3> indexed_vertices;
	    GLuint vertexbuffer;
	    GLuint elementbuffer;
	    GLuint VertexArrayID;

		std::vector<std::string> meshes;
		int nbMeshes;
		std::vector<std::vector<unsigned short>>* trianglesResolution;
	    std::vector<glm::vec3>* indexed_verticesResolution;
		std::vector<unsigned short>* indicesResolution;
		int distMin;
		glm::vec3 vitesse;

		bool isHighParent;
		std::vector<GameObject*> childs;
		GameObject* parent;
		Transform transformation;

	public:
		GameObject();
		GameObject(std::string filename);
		GameObject(std::string filename, bool isHighParent);
		GameObject(std::vector<std::string> filenames, int distMin);
		GameObject(std::vector<std::string> filenames, bool isHighParent, int distMin);

		void setup();
		void draw(GLint programID);

		void loadLOD();
		void setupLOD(int indexMesh);
		void drawLOD(GLint programID, int indexMesh);
		void setParent(GameObject* newParent);
		GameObject* getParent();
		void addChild(GameObject* newChild);
		Transform* getTransform();
		void setTransform(glm::vec3 pos, glm::vec3 rotate, glm::vec3 scale);
		void updatePositionByVitesse(float deltaTime);
		void updateVitesse(glm::vec3 force, float deltaTime);

		std::vector<glm::vec3> getPoints();

		int getDistMin();
		int getNbMeshes();
		glm::vec3 getVitesse();
		void setVitesse(glm::vec3 vitesse);

		virtual ~GameObject();

};

#endif