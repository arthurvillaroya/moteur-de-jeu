#ifndef UTILITES_H
#define UTILITES_H

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>
// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <string>

float randomFloat();
int randomInt(int a, int b);
float randomFloat(int a, int b);

#endif