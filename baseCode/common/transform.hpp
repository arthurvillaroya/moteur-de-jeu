#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <glm/gtc/matrix_transform.hpp>

class Transform{
    
    protected:
        glm::vec3 position;
        glm::vec3 rotate;
        glm::vec3 scale;
        glm::mat4 modelMatrix;

    public:
        
        Transform();
        Transform(glm::vec3 pos, glm::vec3 rotate, glm::vec3 scale);
        void setTransform(glm::vec3 pos, glm::vec3 rotate, glm::vec3 scale);

        glm::vec3 getPosition();
        glm::vec3 getRotate();
        glm::vec3 getScale();

        void setPosition(glm::vec3 position);
        void setRotate(glm::vec3 rotate);
        void setScale(glm::vec3 scale);

        void changePosition(glm::vec3 position);
        void changeRotate(glm::vec3 rotate);
        void changeScale(glm::vec3 scale);
        
        void updateModelMatrix();
        void updateModelMatrix(Transform* parent);
        glm::mat4 getModelMatrix();
};

#endif