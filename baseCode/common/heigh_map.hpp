#ifndef HEIGH_MAP_H
#define HEIGH_MAP_H

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>

class HeighMap{
	public:
		unsigned char * values;
        int width;
        int height;
        int channels;

        HeighMap();
		HeighMap(const char* heighMapPath);
};

#endif