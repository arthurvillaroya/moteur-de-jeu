#ifndef SCENE_H
#define SCENE_H

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include "transform.hpp"
#include "gameObject.hpp"

class Scene{
    
    protected: 
        std::vector<GameObject*> objectChilds;

    public:
        
        Scene();
        void addChild(GameObject* gameO);
        void setupScene();
        void drawScene(GLint programID);



};

#endif
