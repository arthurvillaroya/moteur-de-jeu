#include "gameObject.hpp"

GameObject::GameObject(){
	
}

GameObject::GameObject(std::string filename){
	loadOFF(filename, this->indexed_vertices, this->indices, this->triangles );
}

GameObject::GameObject(std::string filename, bool isHighParent){
	loadOFF(filename, this->indexed_vertices, this->indices, this->triangles );
    this->isHighParent = isHighParent;
    this->transformation = Transform();
}

GameObject::GameObject(std::vector<std::string> filenames, int distMin){
	this->meshes = filenames;
    this->nbMeshes = filenames.size();
    this->distMin = distMin;
}

GameObject::GameObject(std::vector<std::string> filenames, bool isHighParent, int distMin){
    this->distMin = distMin;
	this->meshes = filenames;
    this->nbMeshes = filenames.size();
    this->isHighParent = isHighParent;
    this->transformation = Transform();
}

void GameObject::loadLOD(){
    indexed_verticesResolution = new std::vector<glm::vec3>[nbMeshes];
    indicesResolution = new std::vector<unsigned short>[nbMeshes];
    trianglesResolution = new std::vector<std::vector<unsigned short>>[nbMeshes];
    for(int i = 0; i < this->nbMeshes; i++){
        loadOFF(this->meshes[i], this->indexed_verticesResolution[i], this->indicesResolution[i], this->trianglesResolution[i]);
    }
}

void GameObject::setupLOD(int indexMesh){
    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);

	glGenBuffers(1, &vertexbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glBufferData(GL_ARRAY_BUFFER, indexed_verticesResolution[indexMesh].size() * sizeof(glm::vec3), &indexed_verticesResolution[indexMesh][0], GL_STATIC_DRAW);

    glGenBuffers(1, &elementbuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesResolution[indexMesh].size() * sizeof(unsigned short), &indicesResolution[indexMesh][0] , GL_STATIC_DRAW);

    for(unsigned long int i = 0; i < childs.size(); i++){
        childs[i]->setup();
    }
}

void GameObject::drawLOD(GLint programID, int indexMesh){
	    // 1rst attribute buffer : vertices
        if(isHighParent){
            this->transformation.updateModelMatrix();
        }
        else{
            this->transformation.updateModelMatrix(this->parent->getTransform());
        }
        glUniformMatrix4fv(glGetUniformLocation(programID, "model_mat"), 1, GL_FALSE, &transformation.getModelMatrix()[0][0]);

        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
        glVertexAttribPointer(
                    0,                  // attribute
                    3,                  // size
                    GL_FLOAT,           // type
                    GL_FALSE,           // normalized?
                    0,                  // stride
                    (void*)0            // array buffer offset
                    );

        // Index buffer
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

        // Draw the triangles !
        glDrawElements(
                    GL_TRIANGLES,     // mode
                    indicesResolution[indexMesh].size(),    // count
                    GL_UNSIGNED_SHORT,   // type
                    (void*)0           // element array buffer offset
                    );

        glDisableVertexAttribArray(0);

        for(unsigned long int i = 0; i < childs.size(); i++){
            childs[i]->draw(programID);
        }
}

void GameObject::draw(GLint programID){
	    // 1rst attribute buffer : vertices
        if(isHighParent){
            this->transformation.updateModelMatrix();
        }
        else{
            this->transformation.updateModelMatrix(this->parent->getTransform());
        }
        glUniformMatrix4fv(glGetUniformLocation(programID, "model_mat"), 1, GL_FALSE, &transformation.getModelMatrix()[0][0]);

        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
        glVertexAttribPointer(
                    0,                  // attribute
                    3,                  // size
                    GL_FLOAT,           // type
                    GL_FALSE,           // normalized?
                    0,                  // stride
                    (void*)0            // array buffer offset
                    );

        // Index buffer
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

        // Draw the triangles !
        glDrawElements(
                    GL_TRIANGLES,     // mode
                    indices.size(),    // count
                    GL_UNSIGNED_SHORT,   // type
                    (void*)0           // element array buffer offset
                    );

        glDisableVertexAttribArray(0);

        for(unsigned long int i = 0; i < childs.size(); i++){
            childs[i]->draw(programID);
        }
}

void GameObject::setup(){
	glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);

	glGenBuffers(1, &vertexbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glBufferData(GL_ARRAY_BUFFER, indexed_vertices.size() * sizeof(glm::vec3), &indexed_vertices[0], GL_STATIC_DRAW);

    glGenBuffers(1, &elementbuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0] , GL_STATIC_DRAW);

    for(unsigned long int i = 0; i < childs.size(); i++){
        childs[i]->setup();
    }
}

std::vector<glm::vec3> GameObject::getPoints(){
    return this->indexed_vertices;
}

GameObject* GameObject::getParent(){
    return this->parent;
}

void GameObject::setParent(GameObject* newParent){
    this->parent = newParent;
}

void GameObject::addChild(GameObject* newChild){
    this->childs.push_back(newChild);
}

Transform* GameObject::getTransform(){
    return &this->transformation;
}

void GameObject::setTransform(glm::vec3 pos, glm::vec3 rotate, glm::vec3 scale){
    this->transformation.setTransform(pos, rotate, scale);
}

int GameObject::getDistMin(){
    return this->distMin;
}

int GameObject::getNbMeshes(){
    return this->nbMeshes;
}

glm::vec3 GameObject::getVitesse(){
    return this->vitesse;
}

void GameObject::setVitesse(glm::vec3 vitesse){
    this->vitesse = vitesse;
}

void GameObject::updatePositionByVitesse(float deltaTime){
    glm::vec3 newPos = this->transformation.getPosition() + deltaTime * this->vitesse;
    this->transformation.setPosition(newPos);
}

void GameObject::updateVitesse(glm::vec3 force, float deltaTime){
    this->vitesse = this->vitesse + deltaTime * force;
}

GameObject::~GameObject(){
	glDeleteBuffers(1, &vertexbuffer);
    glDeleteBuffers(1, &elementbuffer);
    glDeleteVertexArrays(1, &VertexArrayID);
}

