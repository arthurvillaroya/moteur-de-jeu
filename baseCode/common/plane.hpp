#ifndef PLANE_H
#define PLANE_H

#include "gameObject.hpp"
#include "texture.hpp"
#include "shader.hpp"
#include "heigh_map.hpp"

class Plane : public GameObject{
	private:
		double longueur;
		double largeur;
		int nbPointsLongueur;
		int nbPointsLargeur;
		glm::vec3 position;
		std::vector<glm::vec2> uvs;
		GLuint UVbuffer;
		HeighMap heighMap;
		GLint m_program = LoadShaders("../TP1/vertex_shader.glsl", "../TP1/fragment_shader.glsl");
		GLint m_textureHerbe;
		GLint m_textureRoche;
		GLint m_textureNeige;

	public:
		Plane();
		Plane(double longueur, double largeur, bool isHighParent);
		Plane(double longueur, double largeur, glm::vec3 position, bool isHighParent);
		Plane(double longueur, double largeur, int nbPointsLongueur, int nbPointsLargeur, glm::vec3 position, bool isHighParent);
		Plane(double longueur, double largeur, int nbPointsLongueur, int nbPointsLargeur, glm::vec3 position, const char*  HeighMapPath, bool isHighParent);

		void loadTextures(std::vector<std::string> colorTextures);
		void construct(bool harsardY, bool heighMapActivated);
		void constructSimple();
		void keys(GLFWwindow *window, bool hasardY, bool heighMapActivated);
		void bindTextures();
		void getUV();
		void loadUVShader();


		/*******GETTERS*********/
		double getLongueur();
		double getLargeur();
		int getNbPointsLongueur();
		int getNbPointsLargeur();
		glm::vec3 getPosition();

		/*******SETTERS*********/
		void setLongueur(double newLongueur);
		void setLargeur(double newLargeur);
		void setNbPointsLongeur(int newNbPointsLongeur);
		void setNbPointsLargeur(int newNbPointsLargeur);
		void setPosition(glm::vec3 newPosition);
};

#endif