#ifndef CONTROLS_HPP
#define CONTROLS_HPP

void computeMatricesFromInputs();
glm::mat4 getViewMatrix();
glm::mat4 getProjectionMatrix();
void setCamPosition(glm::vec3 newPosition);
void setVerticalAngle(float newAngle);
void setHorizontalAngle(float newAngle);


#endif