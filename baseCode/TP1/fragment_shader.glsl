#version 330 core

// Ouput data
out vec4 color;

//uniform int nbTextures;

uniform sampler2D textureHerbe;
uniform sampler2D textureRoche;
uniform sampler2D textureNeige;

uniform bool estPlan;

uniform float hateurMax;

in vec2 uv0;
in vec3 positionGLobale;

void main(){
        
        if(estPlan){
                if(positionGLobale.y < 4){
                        color = texture2D(textureHerbe, uv0).rgba;
                }
                else if(positionGLobale.y < 8){
                        color = texture2D(textureRoche, uv0).rgba;
                }
                else{
                        color = texture2D(textureNeige, uv0).rgba;
                }
        }else{
                color = vec4(0.7, 0.4,0.2, 1);
        }
}
