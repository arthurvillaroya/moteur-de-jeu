#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 vertices_position_modelspace;
layout(location = 1) in vec2 uv;

//TODO create uniform transformations matrices Model View Projection
uniform mat4 model_mat;
uniform mat4 view_mat;
uniform mat4 proj_mat;

out vec2 uv0;
out vec3 positionGLobale;

mat4 transform;
// Values that stay constant for the whole mesh.

void main(){

        uv0.x =  uv.x ;
        uv0.y =  uv.y ;
        // TODO : Output position of the vertex, in clip space : MVP * position
        transform = (proj_mat * view_mat * model_mat);
        positionGLobale = vertices_position_modelspace;
        gl_Position = transform * vec4(vertices_position_modelspace,1);

}

