// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <string>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>
GLFWwindow* window;

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>

using namespace glm;

#include <common/shader.hpp>
#include <common/objloader.hpp>
#include <common/vboindexer.hpp>
#include <common/gameObject.hpp>
#include <common/plane.hpp>
#include <common/controls.hpp>
#include <common/scene.hpp>
#include <common/transform.hpp>

void processInput(GLFWwindow *window);
void rotationInput(GLFWwindow *window, glm::mat4 &ModelMatrix, glm::mat4 &ViewMatrix, bool &orbit, float &rotation, glm::vec3 objectPosition);
int whichMesh(glm::vec3 camera_position, glm::vec3 object_position, int distMin, int nbMeshes);
glm::vec3 pointProche(glm::vec3 obj_pos, std::vector<glm::vec3> points, glm::vec3 distMin, bool XetZdiff);
double clipMapHeight(glm::vec3 obj_pos, std::vector<glm::vec3> points);
glm::vec3 ratioBarycentre(glm::vec3 obj_pos, glm::vec3 &A, glm::vec3 &B, glm::vec3 &C);
glm::vec3 reflectVector(glm::vec3 normal, glm::vec3 vMinus);

// settings
const unsigned int SCR_WIDTH = 1024;
const unsigned int SCR_HEIGHT = 768;

// camera
glm::vec3 camera_position   = glm::vec3(0.0f, 5.0f,  3.0f);
glm::vec3 camera_target = glm::vec3(0.0f, 0.0f, -1.0f);
glm::vec3 camera_up    = glm::vec3(0.0f, 1.0f,  0.0f);

// timing
float deltaTime = 0.0f;	// time between current frame and last frame
float lastFrame = 0.0f;

//rotation
float angle = 0.;
float zoom = 1.;
/*******************************************************************************/

int main( void )
{
    // Initialise GLFW
    if( !glfwInit() )
    {
        fprintf( stderr, "Failed to initialize GLFW\n" );
        getchar();
        return -1;
    }

    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


    window = glfwCreateWindow( 1024, 768, "TP1 - GLFW", NULL, NULL);
    if( window == NULL ){
        fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
        getchar();
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);


    glewExperimental = true; 
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        getchar();
        glfwTerminate();
        return -1;
    }

    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);


    glfwPollEvents();
    glfwSetCursorPos(window, 1024/2, 768/2);


    glClearColor(0.8f, 0.8f, 0.8f, 0.0f);

    glEnable(GL_DEPTH_TEST);

    glDepthFunc(GL_LESS);


    GLuint programID = LoadShaders( "vertex_shader.glsl", "fragment_shader.glsl" );
	
    std::vector<std::string> meshesLapinou;
    meshesLapinou.push_back("bunny.off"); meshesLapinou.push_back("bunny10.off"); 
    meshesLapinou.push_back("bunny5.off"); meshesLapinou.push_back("bunny3.off");
    GameObject Lapinou = GameObject(meshesLapinou, true, 15);
    Lapinou.loadLOD();
    Lapinou.setupLOD(0);

    Plane plan = Plane(30,30,60,60,glm::vec3(0,0,0), "../textures/Heightmap_Mountain.png", true);
    plan.construct(false, false);
    plan.setup();
    plan.getUV();

    GameObject objetMouvant = GameObject("box.off", true);
    objetMouvant.setup();
    objetMouvant.setTransform(glm::vec3(0,0.5 ,0),glm::vec3(0,0,0),glm::vec3(1,1,1));
    objetMouvant.setVitesse(glm::vec3(3,10,0)); 


    double lastTime = glfwGetTime();
    int nbFrames = 0;

    std::vector<std::string> textures;
    std::vector<std::string> stylax; stylax.push_back("stylax.png");
    
    textures.push_back("../textures/grass.png");
    textures.push_back("../textures/rock.png");
    textures.push_back("../textures/snowrocks.png");

    plan.loadTextures(textures);
    

    bool orbit = false;
    float rotation = 0.01;
    glm::mat4 ModelMatrix = glm::mat4(1.0f);

    bool estPlan;
    bool isJumping = false;

    do{
        estPlan = true;

        float currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;
        processInput(window);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glUseProgram(programID);

        glm::mat4 Model = plan.getTransform()->getModelMatrix();
        glm::mat4 ViewMatrix = getViewMatrix();
        glm::mat4 ProjectionMatrix = getProjectionMatrix();

        rotationInput(window, Model, ViewMatrix, orbit, rotation, plan.getPosition());
   	    glUniformMatrix4fv(glGetUniformLocation(programID, "model_mat"), 1, GL_FALSE, &ModelMatrix[0][0]);
    	glUniformMatrix4fv(glGetUniformLocation(programID, "view_mat"), 1, GL_FALSE, &ViewMatrix[0][0]);
    	glUniformMatrix4fv(glGetUniformLocation(programID, "proj_mat"), 1, GL_FALSE, &ProjectionMatrix[0][0]);

        glUniform1i(glGetUniformLocation(programID, "estPlan"), estPlan);
        plan.keys(window, false, false);
        plan.bindTextures();
        plan.loadUVShader();
        plan.draw(programID);

        estPlan = false;
        glUniform1i(glGetUniformLocation(programID, "estPlan"), estPlan);
        /*int actualMeshe = 0;
        int newMeshe;
        newMeshe = whichMesh(camera_position, Lapinou.getTransform()->getPosition(), Lapinou.getDistMin(), Lapinou.getNbMeshes());
        if(actualMeshe != newMeshe){
            Lapinou.setupLOD(newMeshe);
            actualMeshe = newMeshe;
        }
        Lapinou.drawLOD(programID, actualMeshe);*/

        if(glfwGetKey(window, GLFW_KEY_LEFT)){
            objetMouvant.getTransform()->changePosition(glm::vec3(-0.5,0,0));
        }
        if(glfwGetKey(window, GLFW_KEY_RIGHT)){
            objetMouvant.getTransform()->changePosition(glm::vec3(0.5,0,0));
        }
        if(glfwGetKey(window, GLFW_KEY_UP)){
            objetMouvant.getTransform()->changePosition(glm::vec3(0,0,-0.5));
        }
        if(glfwGetKey(window, GLFW_KEY_DOWN)){
            objetMouvant.getTransform()->changePosition(glm::vec3(0,0,0.5));
        }
        if(glfwGetKey(window, GLFW_KEY_J)){
            isJumping = true;
            objetMouvant.setVitesse(glm::vec3(3,10,0)); 
        }
        if(isJumping){
            objetMouvant.updatePositionByVitesse(deltaTime);
            objetMouvant.updateVitesse(glm::vec3(0,-9.8,0), deltaTime);
        }
        if((objetMouvant.getTransform()->getPosition().y - 0.5) <= 0.0){
            glm::vec3 rebond = 0.5f * reflectVector(glm::vec3(0,1,0), objetMouvant.getVitesse());
            objetMouvant.setVitesse(rebond);
            objetMouvant.getTransform()->changePosition(glm::vec3(0,0.1,0));
        }


        //objetMouvant.getTransform()->setPosition(glm::vec3(objetMouvant.getTransform()->getPosition().x,clipMapHeight(objetMouvant.getTransform()->getPosition(), plan.getPoints())+1, objetMouvant.getTransform()->getPosition().z));
        objetMouvant.draw(programID);
        
        computeMatricesFromInputs();

        glfwSwapBuffers(window);
        glfwPollEvents();
    } 
    while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
           glfwWindowShouldClose(window) == 0 );

    glDeleteProgram(programID);

    glfwTerminate();

    return 0;
}

void processInput(GLFWwindow *window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);

    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

    //Camera zoom in and out
    float cameraSpeed = 2.5 * deltaTime;
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        camera_position += cameraSpeed * camera_target;
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        camera_position -= cameraSpeed * camera_target;
    //TODO add translations
}

void rotationInput(GLFWwindow *window, glm::mat4 &ModelMatrix, glm::mat4 &ViewMatrix, bool &orbit, float &rotation, glm::vec3 objectPosition){

    float horizontalAngle = 3.14f;
    // Initial vertical angle : none
    float verticalAngle = 0.0f;

    glm::vec3 right = glm::vec3(
		sin(horizontalAngle - 3.14f/2.0f), 
		0,
		cos(horizontalAngle - 3.14f/2.0f)
	);

    if (glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS && !orbit){
        orbit = true;
    }
    else if(glfwGetKey(window, GLFW_KEY_V) == GLFW_PRESS && orbit){
        orbit = false;
    }

    if(orbit){
        setCamPosition(glm::vec3(0,50,50));
        setVerticalAngle(-3.14f/4.0f);
        ModelMatrix = glm::rotate(ModelMatrix, rotation, glm::vec3(0,1,0));
    }
    else{   
        ModelMatrix = ModelMatrix;
    }

    if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS){
        rotation += 0.002;
    }
    else if(glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS){
        rotation -= 0.002;
    }
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
}

int whichMesh(glm::vec3 camera_position, glm::vec3 object_position, int distMin, int nbMeshes){
    double distCam = sqrt((camera_position.x - object_position.x)*(camera_position.x - object_position.x) +
                          (camera_position.y - object_position.y)*(camera_position.y - object_position.y) +
                          (camera_position.z - object_position.z)*(camera_position.z - object_position.z));

    double ratio = (double)nbMeshes / distMin;
    
    int whichmesh = (int)(ratio*distCam); 
    
    if(whichmesh >= nbMeshes){return nbMeshes - 1;}
    else{
        return whichmesh;
    }
}

glm::vec3 pointProche(glm::vec3 obj_pos, std::vector<glm::vec3> points, glm::vec3 pointMin, bool XetZdiff){
    double distanceGoal = std::numeric_limits<double>::max();
    int index = 0;

    for(unsigned int i = 0; i < points.size(); i++){
        double distancePointObj = sqrt((obj_pos.x-points[i].x)*(obj_pos.x-points[i].x)+(obj_pos.z-points[i].z)*(obj_pos.z-points[i].z));
        if(distancePointObj <= distanceGoal && points[i] != pointMin){
            if(XetZdiff){
                if(points[i].x != pointMin.x && points[i].z != pointMin.z){
                    distanceGoal = distancePointObj;
                    index = i;
                }
            }else{
                distanceGoal = distancePointObj;
                index = i;
            }
        }
    }

    return points[index];
}

double clipMapHeight(glm::vec3 obj_pos, std::vector<glm::vec3> points){
    std::vector<glm::vec3> triangle;
    triangle.push_back(pointProche(obj_pos, points, obj_pos, false));
    triangle.push_back(pointProche(obj_pos, points, triangle[0], false));
    triangle.push_back(pointProche(obj_pos, points, triangle[1], true));

    glm::vec3 bary = ratioBarycentre(obj_pos, triangle[0], triangle[1], triangle[2]);

    return (triangle[0].y*bary.z + triangle[1].y*bary.x + triangle[2].y*bary.y);
}

glm::vec3 ratioBarycentre(glm::vec3 obj_pos, glm::vec3 &A, glm::vec3 &B, glm::vec3 &C){
    glm::vec3 AB = B - A;
    glm::vec3 AC = C - A;
    glm::vec3 AP = obj_pos - A;

    double det = glm::length(glm::cross(AC, AB));

    double alpha = glm::length(glm::cross(AC, AP))*det;
    double beta = glm::length(glm::cross(AP, AB))*det;
    double gamma = 1.0 - alpha - beta;
    
    return glm::vec3(alpha, beta, gamma); 
}

glm::vec3 reflectVector(glm::vec3 normal, glm::vec3 vMinus){
    //normal is normalized

    return vMinus - (2.0f * glm::dot(vMinus, normal)) * normal;
}
